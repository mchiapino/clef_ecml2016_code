import numpy as np

import clef_algo as clf


####################
# Script functions #
####################

def remove_doublon(x_faces):
    x_new_faces = np.array([x_faces[0, :]])
    list_faces = [list(np.nonzero(x_faces[0, :])[0])]
    for face in x_faces[1:, :]:
        size_face = np.sum(face)
        size_new_faces = np.sum(x_new_faces, axis=1)
        s_1 = set(np.nonzero(np.sum(x_new_faces * face,
                                    axis=1) == size_face)[0])
        s_2 = set(np.nonzero(size_new_faces == size_face)[0])
        if len(s_1 & s_2) == 0:
            x_new_faces = np.concatenate((x_new_faces, np.array([face])))
            list_faces.append(list(np.nonzero(face)[0]))

    return x_new_faces, list_faces


def mass_faces(x_damex):
    n_extr = len(x_damex)
    x_damex_faces, damex_faces = remove_doublon(x_damex)
    n_faces = len(x_damex_faces)
    size_damex = np.sum(x_damex, axis=1)
    size_damex_faces = np.sum(x_damex_faces, axis=1)
    intersect = np.dot(x_damex, x_damex_faces.T)
    mass = np.zeros(n_faces)
    for k, size_face in enumerate(size_damex_faces):
        ind = np.nonzero(size_damex == size_face)[0]
        mass[k] = np.sum(intersect[ind, k] == size_face)/float(n_extr)

    return mass


##########
# Script #
##########

R = 600
x_rank = np.load('hydro_data/normalized_discharge_997.npy')
x_extr = clf.extrem_points(x_rank, R)
n_extr, n_dim = np.shape(x_extr)

epss = [0.01, 0.05, 0.1, 0.2]
table2 = np.zeros((4, 4))
for k, eps in enumerate(epss):
    x_damex = (x_extr > eps * np.max(x_extr, axis=1)[np.newaxis].T)
    mass = mass_faces(x_damex)
    nb_faces = len(mass)
    nb_faces_5 = np.sum(mass > 5/float(n_extr))
    nb_faces_1pct = 100*np.sum(mass < 0.01)/float(nb_faces)
    table2[k, :] = np.array([eps, nb_faces, nb_faces_5, nb_faces_1pct])
np.set_printoptions(precision=2, suppress=True)
print table2
