import numpy as np

import clef_algo as clf


####################
# Script functions #
####################

def compute_tau(list_alphas, x_bin):
    n_extr, n_dim = np.shape(x_bin)
    x_alphas = clf.list_alphas_to_vect(list_alphas, n_dim)
    tau = np.sum(np.sum(np.dot(x_bin, x_alphas.T) >=
                        np.sum(x_alphas, axis=1) - 1,
                        axis=1) > 0)/float(n_extr)

    return tau


##########
# Script #
##########

R = 600
x_rank = np.load('hydro_data/normalized_discharge_997.npy')
x_extr = clf.extrem_points(x_rank, R)
x_bin = clf.above_thresh_binary(x_extr, R)

mus = [0.4, 0.35, 0.3, 0.25]
table1 = np.zeros((4, 4))
for k, mu in enumerate(mus):
    A = clf.find_alphas(x_bin, mu)
    maximal_alphas = clf.find_maximal_alphas(A)
    list_alphas = [alpha for alphas in maximal_alphas for alpha in alphas]
    nb_alphas = len(list_alphas)
    quantile = np.percentile(map(len, list_alphas), 90)
    tau = compute_tau(list_alphas, x_bin)
    table1[k, :] = np.array([mu, nb_alphas, quantile, tau])

print table1
