import numpy as np

import clef_algo as clf
import simul_multivar_evd as sevd

import fim
import pickle

####################
# Script fucntions #
####################


def check_errors(charged_alphas, result_alphas, dim):
    """
    Alphas founds -> Alphas (recovered, misseds, falses)
    """
    n = len(result_alphas)
    x_true = clf.list_alphas_to_vect(charged_alphas, dim)
    x = clf.list_alphas_to_vect(result_alphas, dim)
    # Find supsets of real alpha
    true_lengths = np.sum(x_true, axis=1)
    cond_1 = np.dot(x, x_true.T) == true_lengths
    ind_supsets = np.nonzero(np.sum(cond_1, axis=1))[0]
    # Find subsets of a real alpha
    res_lengths = np.sum(x, axis=1)
    cond_2 = np.dot(x_true, x.T) == res_lengths
    ind_subsets = np.nonzero(np.sum(cond_2.T, axis=1))[0]
    # Intersect sub and supsets to get recovered alphas
    cond = cond_1 * cond_2.T
    ind_recov = np.nonzero(np.sum(cond, axis=1))[0]
    ind_exct_supsets = list(set(ind_supsets) - set(ind_recov))
    ind_exct_subsets = list(set(ind_subsets) - set(ind_recov))
    set_ind = set(ind_recov) | set(ind_exct_supsets) | set(ind_exct_subsets)
    ind_pure_false = list(set(range(n)) - set_ind)
    # Results
    founds = [result_alphas[i] for i in ind_recov]
    falses_pure = [result_alphas[i] for i in ind_pure_false]
    exct_subsets = [result_alphas[i] for i in ind_exct_subsets]
    exct_supsets = [result_alphas[i] for i in ind_exct_supsets]
    ind_misseds = np.nonzero(np.sum(cond, axis=0) == 0)[0]
    misseds = [charged_alphas[i] for i in ind_misseds]

    return founds, misseds, falses_pure, exct_subsets, exct_supsets


def find_R(x_sim, R_0, eps):
    R = R_0
    n_exrt = len(clf.extrem_points(x_sim, R))
    while n_exrt > eps*len(x_sim):
        R += 250
        n_exrt = len(clf.extrem_points(x_sim, R))

    return R


def check_dataset(dataset):
    """
    binary dataset -> nb of points per subfaces
    """
    n_sample, n_dim = np.shape(dataset)
    n_extr_feats = np.sum(dataset, axis=1)
    n_shared_feats = np.dot(dataset, dataset.T)
    exact_extr_feats = (n_shared_feats == n_extr_feats) * (
        n_shared_feats.T == n_extr_feats).T
    feat_non_covered = set(range(n_sample))
    samples_nb = {}
    for i in xrange(n_sample):
        feats = list(np.nonzero(exact_extr_feats[i, :])[0])
        if i in feat_non_covered:
            feat_non_covered -= set(feats)
            if n_extr_feats[i] > 1:
                samples_nb[i] = len(feats) + 1

    return samples_nb


########
# Main #
########


dim = 100
nb_faces = 70

# # Datasets
# print 'generating datasets..'
# max_size = 8
# p_geom = 0.3
# n_datasets = 20
# n_samples = int(10e4)
# alphas = {}
# datasets = {}
# for i in xrange(n_datasets):
#     print i, '/', n_datasets
#     list_charged_faces = sevd.random_alphas(dim, nb_faces, max_size, p_geom)
#     x_bin = sevd.asym_logistic_noise_anr(dim, list_charged_faces, n_samples)
#     alphas[i] = list_charged_faces
#     datasets[i] = x_bin
# data = {}
# data['alphas'] = alphas
# data['datasets'] = datasets
# data_filename = 'logistic_' + str(dim) + '_' + str(nb_faces) + '.p'
# with open(data_filename, 'wb') as f:
#     pickle.dump(data, f)

# # Bin datasets
# data_filename = 'logistic_' + str(dim) + '_' + str(nb_faces) + '.p'
# with open(data_filename, 'rb') as f:
#     data = pickle.load(f)
# datasets = data['datasets']
# alphas = data['alphas']
# loops = datasets.keys()
# n_loops = len(loops)
# bin_datasets = {}
# for l in xrange(n_loops):
#     x_sim = datasets[l]
#     R_0 = 250
#     R = find_R(x_sim, R_0, 0.05)
#     x_extr = clf.extrem_points(x_sim, R)
#     x_bin = clf.above_thresh_binary(x_extr, R)
#     bin_datasets[l] = x_bin
# data = {}
# data['alphas'] = alphas
# data['datasets'] = bin_datasets
# data_filename = 'logistic_' + str(dim) + '_' + str(nb_faces) + '_bin.p'
# with open(data_filename, 'wb') as f:
#     pickle.dump(data, f)

# Load data bin
data_filename = 'logistic_' + str(dim) + '_' + str(nb_faces) + '_bin.p'
with open(data_filename, 'rb') as f:
    data = pickle.load(f)
datasets = data['datasets']
alphas = data['alphas']

# Test clef
print 'clef..'
mu = 0.035
loops = datasets.keys()
n_loops = len(loops)
clef_results = np.zeros((n_loops, 2))
for l in xrange(n_loops):
    x_bin = datasets[l]
    charged_alphas = alphas[l]
    A = clf.find_alphas(x_bin, mu)
    maximal_alphas_ = clf.find_maximal_alphas(A)
    maximal_alphas = [alpha for alphas_k in maximal_alphas_ for
                      alpha in alphas_k]
    clef_results[l, :] = map(len, check_errors(charged_alphas,
                                               maximal_alphas, dim))
    clef_result = np.mean(clef_results, axis=0)
clef_filename = 'clef_' + str(dim) + '_' + str(nb_faces) + '.p'
with open(clef_filename, 'wb') as f:
    pickle.dump(clef_result, f)

# Check clef
clef_filename = 'clef_' + str(dim) + '_' + str(nb_faces) + '.p'
with open(clef_filename, 'rb') as f:
    clef_result = pickle.load(f)
print 'average number of error:', sum(clef_result[1:5])
print 'recovered clusters:', clef_result[0], '/', nb_faces

# # Test fim
# print 'apriori..'
# supp = 0.135
# loops = datasets.keys()
# n_loops = len(loops)
# fim_results = np.zeros((n_loops, 5))
# for l in loops:
#     x_bin = datasets[l]
#     x_bin_fim = [list(np.nonzero(x)[0]) for x in x_bin]
#     charged_alphas = alphas[l]
#     result_fim = fim.apriori(x_bin_fim, target='m', supp=supp, zmin=2)
#     maximal_alphas = [list(res[0]) for res in result_fim]
#     fim_results[l, :] = map(len, check_errors(charged_alphas,
#                                               maximal_alphas, dim))
#     fim_result = np.mean(fim_results, axis=0)
# fim_filename = 'fim_' + str(dim) + '_' + str(nb_faces) + '.p'
# with open(fim_filename, 'wb') as f:
#     pickle.dump(fim_result, f)

# # Check fim
# fim_filename = 'fim_' + str(dim) + '_' + str(nb_faces) + '.p'
# with open(fim_filename, 'rb') as f:
#     fim_result = pickle.load(f)
# print 'average number of error:', sum(fim_result[1:5])
# print 'recovered clusters:', fim_result[0], '/', nb_faces

# # Load data
# data_filename = 'logistic_' + str(dim) + '_' + str(nb_faces) + '.p'
# with open(data_filename, 'rb') as f:
#     data = pickle.load(f)
# datasets_raw = data['datasets']

# # Test Damex
# print 'damex..'
# eps = 0.3
# loops = datasets.keys()
# n_loops = len(loops)
# damex_results = np.zeros((n_loops, 5))
# for l in loops:
#     x_sim = datasets_raw[l]
#     R = find_R(x_sim, 250, 0.05)
#     x_extr = clf.extrem_points(x_sim, R)
#     x_bin = 1*(x_extr.T > eps*np.max(x_extr, axis=1)).T
#     x_bin = x_bin[np.nonzero(np.sum(x_bin, axis=1) > 1)[0]]
#     n_extr = len(x_bin)
#     res = check_dataset(x_bin)
#     charged_alphas = alphas[l]
#     alphas_res = [list(np.nonzero(x_bin[res.keys()[i], :])[0])
#                   for i in np.argsort(res.values())[::-1][:nb_faces]]
#     damex_results[l, :] = map(len, check_errors(charged_alphas,
#                                                 alphas_res, dim))
#     damex_result = np.mean(damex_results, axis=0)
# damex_filename = 'damex_' + str(dim) + '_' + str(nb_faces) + '.p'
# with open(damex_filename, 'wb') as f:
#     pickle.dump(damex_result, f)

# # Check damex
# damex_filename = 'damex_' + str(dim) + '_' + str(nb_faces) + '.p'
# with open(damex_filename, 'rb') as f:
#     damex_result = pickle.load(f)
# print 'average number of error:', sum(damex_result[1:5])
# print 'recovered clusters:', damex_result[0], '/', nb_faces
